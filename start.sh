#!/bin/bash
echo "Buscando puertos abiertos"
puerto=9003
comando="sudo netstat -lptn | grep LISTEN"
if [ "Darwin" == $(uname -a | cut -d" " -f1) ]
then
comando="sudo lsof -PiTCP -sTCP:LISTEN"
fi
abierto=$(eval "$comando | grep ${puerto} | wc -l")
while [ ${abierto} == 1 ]
do
((puerto++))
abierto=$(eval "$comando | grep ${puerto} | wc -l")
done
app="apimeli"

echo "Contruyendo la imagen de ser necesario"
docker build -t ${app} .

echo "Levantando conainer ${app}${puerto}"
docker run -d -p ${puerto}:80 \
  --name=${app}${puerto} \
  --network $(docker network ls | grep _proxy | cut -d" " -f9) \
  -v $PWD:/app ${app}

docker exec "$(docker container ls -a --format '{{.Names}}' | grep nginx-proxy)" sh -c "echo 'server $app$puerto;' >> /etc/nginx/conf.d/servers.lst"
docker exec "$(docker container ls -a --format '{{.Names}}' | grep nginx-proxy)" sh -c "nginx -s reload"
