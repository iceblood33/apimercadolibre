from flask import Flask
from flask_caching import Cache
import requests
import os

config = {
    "DEBUG": True,
    "CACHE_TYPE": "simple",
    "CACHE_DEFAULT_TIMEOUT": 300
}

app = Flask(__name__)
app.config.from_pyfile(os.path.join(app.root_path, 'config', 'settings.py'))
app.config.from_mapping(config)
cache = Cache(app)

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
@cache.cached(timeout=5000)
def contenido(path):
    return requests.get(url=app.config.get('URL_API')+"/"+path).content

if __name__ == '__main__':
    app.run()
